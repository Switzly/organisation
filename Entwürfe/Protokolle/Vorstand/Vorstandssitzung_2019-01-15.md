Zeit: 15.1.2019, Start: 21:45 Uhr, Ende: 23:45 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Malte
- Gregor
- Dennis

Arne fehlt entschuldigt.

---

# [TOP 1] Formalia

- Vorstand in beschlussfähiger Anzahl anwesend
- Einladung erfolgte am 8.1.2019 fristgerecht per Internet und persönlich.

- Das Protokoll der Vorstandssitzung vom 13.11.2018
  wurde angenommen und unterzeichnet.

- Die Tagesordnung wurde einstimmig angenommen.

# [TOP 2] Protokoll MV 2018

- Entwurf für Protokoll liegt vor
- Protokoll muss von Gregor (Sitzungsleiter) und Oliver (Schriftführer) unterschrieben werden
- Gregor plant Treffen mit Oliver um das Protokoll zu finalisieren

# [TOP 3] Formular für Mitgliedsantrag

- Von Dennis entworfenes Formular ist weitestgehend in Ordnung. Außerordentliche Mitglieder soll noch
  in Fördermitglieder geändert werden und vorgeschlagener Beitrag für Fördermitglieder
  auf 250€. Zudem wird ein Feld "ich bin mit der Veröffentlichung meiner Mitgliedschaft" für
  Fördermitglieder eingefügt. Das Formular soll dann ab sofort verwendet werden.

# [TOP 4] MV 2019

- MV wollen wir in der zweiten Märzhälfte durchführen
- Gregor macht ein Doodle o.Ä. um die Mitglieder nach Terminwünschen zu fragen

# [TOP 5] Erstattungen 35C3

- Es liegen 2 Anträge auf Erstattung von Hotelkosten sowie Tickets vor.
  (Eine Eintrittskarte hiervon für 120€ wurde bereits vor dem Kongress bezahlt.)
- Mit 3 Für-Stimmen beschlossen (Gregor nicht stimmberechtigt, da betroffen)
  - Die Hotelrechnungen in Höhe von 197,60€ und 234€ werden erstattet.
  - Zusätzlich zur bereits bezahlten Eintrittskarte wird auch die zweite über 144,31€ bezahlt.

# [TOP 6] Finanzierungsstrategie

- Wir sollten Firmen gezielt ansprechen, ob sie Fördermitglied werden möchten.
  Die Koordination übernimmt Gregor. Fördermitglieder werden im Gitlab geführt.

# [TOP 7] Server

- Vom Verein finanzierte Server sollten Eigentum des Vereins oder offiziell gemietet sein.
- Bei den Servern, die uns bisher kostenlos zur Verfügung gestellt werden,
  fragen wir an, ob und zu welchen Konditionen der Verein die Server kaufen kann.
- Die kleineren beiden Server sind nahe ihrer Kapazitätsgrenze. Wir sollten überlegen,
  sie gegen einen oder mehrere leistungsstärkere Server zu ersetzen.

# [TOP 8] CPDP Computer Privacy and Data Protection Konferenz

- Gregor ist aufgefordert worden, auf der CPDP die Problematik um Wifi4EU zu beleuchten.
  Er versucht eine Finanzierung sicherzustellen, würde sich aber freuen, wenn ggf. der
  Verein Reise- und Übernachtungskosten übernehmen könnte.

