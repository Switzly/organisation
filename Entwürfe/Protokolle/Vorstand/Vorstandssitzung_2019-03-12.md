Zeit: 12.3.2019, Start: 22:00 Uhr, Ende: 23:45 Uhr

Ort: Chico Mendes, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Malte
- Gregor
- Dennis

Arne fehlt entschuldigt.

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend
- Einladung erfolgte fristgemäß am 5.3. per Slack.

# [TOP 2] Mitgliederversammlung

- Der Vorstand beschließt einstimmig zur Mitgliederversammlung
  am 11.4.2019 einzuladen und formuliert gemeinsam das Einladungsschreiben
  mit der Tagesordnung.
