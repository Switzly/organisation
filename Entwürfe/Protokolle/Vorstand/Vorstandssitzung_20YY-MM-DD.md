Zeit: 31.8.2018, Start: xx:xx Uhr, Ende: xx:xx Uhr

Ort: Gebäude, Ort

Protokoll: Vorname Nachname / Pseudonym

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP n] Formalia

- Vorstand (un)vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 14.8.2018 bei der Vorstandssitzung.
ODER
- Einladung erfolgte ordnungsgemäß am D.M.20YY bei persönlichem Treffen bzw. 
  per persönlicher telefonischer Rückfrage. Alle Vorstandsmitglieder sind mit der
  verkürzten Einladungsfrist einverstanden.

# [TOP n+1] Foo

- foo
  - bar
- foobar
