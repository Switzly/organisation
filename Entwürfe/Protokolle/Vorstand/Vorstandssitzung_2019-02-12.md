Zeit: 12.2.2019, Start: 21:00 Uhr, Ende: 22:00 Uhr

Ort: Labyrinth, Aachen

Protokoll: Jan Niehusmann

---

__Anwesend__
- Jan
- Malte
- Dennis

__Entschuldigt__
- Gregor
- Arne

---

# [TOP 1] Formalia

- Vorstand unvollständig anwesend - Gregor + Arne sind entschuldigt
- Einladung erfolgte ordnungsgemäß am 5.2.2019 per Mail

# [TOP 2] Fahrtkostenerstattung Ehrenwert

- Eine Mitglied aus Düren bekommt noch die Fahrtkosten zur
  Veranstaltung "Ehrenwert", 9.9.2018 in Höhe von 15€ erstattet.
  (einstimmig beschlossen)

# [TOP 3] Mitgliederversammlung

- Malte macht ein Doodle zur Terminfindung (28.3.-14.4.)
- Beschluss des Termins soll bei einer Vorstandssitzung
  am 26.2. erfolgen, ggf. online / im Umlaufverfahren
- Einladung (Raum!) incl. Satzungsänderung muss bis dahin vorbereitet
  sein
- Jan kümmert sich um Kassenbericht und -prüfung


Ende der Sitzung um 22 Uhr

