# Satzung des Vereins


## Fördervereinigung für freie Netzwerke in der Region Aachen


### §1 Name und Sitz des Vereins

1. Der Verein führt den Namen „Fördervereinigung für freie Netzwerke in der
   "Region Aachen"
2. Er soll in das Vereinsregister eingetragen werden und trägt dann den Zusatz
   „e.V.“.
3. Der Sitz des Vereins ist Aachen.


### §2 Geschäftsjahr

Das Geschäftsjahr ist das Kalenderjahr.


### §3 Zweck des Vereins

Die Zwecke des Vereins sind:
- die Förderung von Wissenschaft und Forschung
- die Förderung der Volks- & Berufsbildung
- die Förderung des bürgerschaftlichen Engagements zugunsten gemeinnütziger,
  mildtätiger und kirchlicher Zwecke.

Der Satzungszweck wird verwirklicht insbesondere durch die Förderung:

1. der Erforschung, Anwendung und Verbreitung freier Netzwerktechnologien
   sowie die Verbreitung und Vermittlung von Wissen über Funk- und
   Netzwerktechnologien.
2. des Zugangs zur Informationstechnologie für sozial benachteiligte Personen,
3. der Schaffung experimenteller Kommunikations- und Infrastrukturen, sowie
   Bürgerdatennetzen,
4. kultureller, technologischer und sozialer Bildungs- und Forschungsobjekte,
5. der Veranstaltung regionaler, nationaler und internationaler Kongresse,
   Treffen und Konferenzen, sowie der Teilnahme der Mitglieder.

Dies soll unter anderem auf folgende Weisen erreicht werden:

1. Im Allgemeinen durch die Unterstützung der Arbeit von Freifunk Rheinland
   e.V. und anderen Organisationen, die in den Bereichen tätig sind:  
   a. Förderung und Erprobung und Erforschung freier Datennetze in
      Pilotprojekten und neuartigen Kommunikationsmöglichkeiten in
      Mesh-Netzwerken.
   b. Förderung neuer Formen digitalen, zivilgesellschaftlichen Engagements für
      offene, dezentrale, nicht-kommerzielle Datennetze.
2. Im Speziellen der Förderung der Freifunk-Communities in der Region.


### §4 Selbstlose Tätigkeit

Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie
eigenwirtschaftliche Zwecke.


### §5 Mittelverwendung

1. Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet
   werden.
2. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.


### §6 Verbot von Vergünstigungen

Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind,
oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.


### §7 Erwerb der Mitgliedschaft

1. Vereinsmitglieder können natürliche und juristische Personen werden.
2. Der Aufnahmeantrag ist an den Vorstand in Textform zu stellen.
3. Über den Aufnahmeantrag entscheidet der Vorstand.
4. Eine Ablehnung bedarf keiner Begründung.
5. Gegen eine Ablehnung steht Bewerber\*innen eine Berufung an die
   Mitgliederversammlung zu, diese entscheidet dann endgültig.


### §8 Arten der Mitgliedschaft

1. Mitglieder des Vereins setzen sich aus ordentlichen Mitgliedern und
   Fördermitgliedern zusammen.  
   1.1. Ordentliche Mitglieder
        Ordentliche Mitglieder können natürliche und juristische Personen sein.  
   1.2. Fördermitglieder
        Fördermitglieder können natürliche und juristische Personen sein.


### §9 Beendigung der Mitgliedschaft

1. Die Mitgliedschaft endet durch Austritt, Ausschluss, Tod sowie Auflösung
   oder Aufhebung der juristischen Person.
2. Der Austritt erfolgt durch Erklärung in Textform gegenüber einem
   Vorstandsmitglied. Die Austrittserklärung muss mit einer Frist von einem
   Monat jeweils zum Ende des Geschäftsjahres gegenüber dem Vorstand erklärt
   werden.
3. Auf Wunsch können ordentliche Mitglieder auch mit sofortiger Wirksamkeit
   austreten.
4. Ein Ausschluss kann nur aus wichtigem Grund erfolgen. Wichtige Gründe sind
   insbesondere:  
   4.1. ein die Vereinsziele schädigendes Verhalten oder  
   4.2. die Verletzung satzungsmäßiger Pflichten.
5. Über den Ausschluss entscheidet der Vorstand.
6. Gegen den Ausschluss steht dem Mitglied eine Berufung an die
   Mitgliederversammlung zu, die schriftlich binnen eines Monats an den
   Vorstand zu richten ist.
7. Vom Zeitpunkt des Einspruchs bis zur Entscheidung über den Ausschluss
   besteht die Mitgliedschaft weiter.
8. Die Mitgliederversammlung entscheidet im Rahmen des Vereins endgültig. Dem
   Mitglied bleibt die Überprüfung der Maßnahme durch Anrufung der ordentlichen
   Gerichte vorbehalten.
9. Die Anrufung eines ordentlichen Gerichts hat aufschiebende Wirkung bis zur
   Rechtskraft der gerichtlichen Entscheidung.


### §10 Ruhende Mitgliedschaften

1. Alle Arten der Mitgliedschaft können ruhend gestellt werden.
2. Ruhendstellung von Mitgliedern:  
   2.1. Ruhendstellung ordentlicher Mitglieder
        Der Vorstand fragt zum Ende des Geschäftsjahres ab, ob ordentliche
        Mitglieder ihre Mitgliedschaft im darauffolgenden Geschäftsjahr
        aufrecht erhalten wollen. Teilt ein ordentliches Mitglied die
        Aufrechterhaltung dem Vorstand bis zwei Wochen nach Beginn des neuen
        Geschäftsjahres nicht mit, so wird es automatisch ein ruhendes
        Mitglied.  
   2.2. Ruhendstellung von Fördermitgliedern
        Fördermitglieder können durch Mitteilung gegenüber dem Vorstand ihre
        Mitgliedschaft ruhend stellen.  
3. Rückmeldung ruhender Mitglieder
   Ruhende Mitglieder können durch schriftliche oder mündliche Rückmeldung oder
   schlüssiges Handeln beim Vorstand ihren Status als Mitglied mit sofortiger
   Wirkung wieder aufleben lassen.
4. Auswirkungen der Ruhendstellung
   Mitglieder, deren Mitgliedschaft ruhend gestellt wurde, haben kein
   Stimmrecht in den Organen des Vereins und werden bei der Ermittlung von
   Quora nicht berücksichtigt.


### §11 Beiträge

1. Von den Mitgliedern werden Beiträge erhoben.
2. Näheres regelt die Beitragsordnung, diese wird von der Mitgliederversammlung
   festgelegt.


### §12 Organe des Vereins

Die Organe des Vereins sind:

1. die Mitgliederversammlung 
2. der Vorstand
3. die Teams
4. der Beirat


### §13 Mitgliederversammlung

1.  Die Mitgliederversammlung ist das oberste Vereinsorgan.
2.  Zu ihren Aufgaben gehören insbesondere:  
    2.1.  die Wahl und Abwahl des Vorstands,  
    2.2.  die Entlastung des Vorstands,  
    2.3.  die Wahl von Kassenprüfer\*innen,  
    2.4.  die Entgegennahme der Berichte des Vorstandes, der Kassenprüfer\*innen
          und der anderen Vereinsorgane,  
    2.5.  die Festsetzung der Beitragsordnung,  
    2.6.  die Beschlussfassung über die Änderung der Satzung und des Zwecks des
          Vereins,  
    2.7.  die Beschlussfassung über die Auflösung des Vereins,  
    2.8.  die Entscheidung über Aufnahme und Ausschluss von Mitgliedern in
          Berufungsfällen,  
    2.9.  die Berufung und Auflösung von Teams,  
    2.10. die Berufung von Beiräten,  
    2.11. die Festsetzung einer Geschäftsordnung der Mitgliederversammlung,  
    2.12. die Festsetzung einer Geschäftsordnung des Vorstandes,  
    2.13. die Festsetzung einer Rahmengeschäftsordnung für Teams,  
    2.14. die Festsetzung einer Geschäftsordnung des Beirats,  
    2.15. die Festsetzung einer Finanzordnung,  
    sowie weitere Aufgaben, soweit sich diese aus der Satzung oder nach dem
    Gesetz ergeben.
3.  Im ersten Quartal eines jeden Geschäftsjahres findet eine ordentliche
    Mitgliederversammlung statt.
4.  Die Mitgliederversammlung wird vom Vorstand unter Einhaltung einer Frist
    von vier Wochen schriftlich oder per E-Mail unter Angabe der Tagesordnung
    einberufen.
5.  Einberufung der Mitgliederversammlung auf Wunsch von Mitgliedern  
    5.1. Der Vorstand ist zur Einberufung einer außerordentlichen
         Mitgliederversammlung verpflichtet, wenn mindestens ein Drittel der
         Mitglieder oder 10% der ordentlichen Mitglieder dies in Textform
         unter Angabe von Gründen verlangt.  
    5.2. Kommt der Vorstand dieser Verpflichtung innerhalb von 2 Wochen nicht
         nach, sind die Vereinsmitglieder befugt, selbst mit einer Frist von
         zwei Wochen schriftlich oder per E-Mail unter Angabe der Tagesordnung
         einzuberufen.  
6.  Die Ladungsfristen beginnen mit dem auf die Absendung des
    Einladungsschreibens oder der Mail folgenden Tag. Das Einladungsschreiben
    gilt als den Mitgliedern zugegangen, wenn es an die letzte dem Verein
    bekannt gegebene Anschrift gerichtet war.
7.  Soll der Vorstand entlastet werden, so ist der Einladung ein
    Kassenprüfbericht der Kassenprüfer\*innen beizulegen.
8.  Die Tagesordnung ist zu ergänzen, wenn dies ein Mitglied bis spätestens
    zwei Wochen vor dem angesetzten Termin in Textform beantragt. Die Ergänzung
    ist 10 Tage vor der Versammlung bekanntzumachen.
9.  Anträge über:  
    9.1. die Abwahl des Vorstands,  
    9.2. die Änderung der Satzung,  
    9.3. die Auflösung des Vereins,  
    die den Mitgliedern nicht bereits mit der Einladung zur
    Mitgliederversammlung zugegangen sind, können erst auf der nächsten
    Mitgliederversammlung beschlossen werden.
10. Die Mitgliederversammlung ist ohne Rücksicht auf die Zahl der erschienenen
    Mitglieder beschlussfähig.
11. Die Mitgliederversammlung wird von einer von der Mitgliederversammlung
    gewählten Person  geleitet.
12. Zu Beginn der Mitgliederversammlung ist ein\*e Schriftführer\*in zu wählen.
13. Jedes ordentliche Mitglied hat eine Stimme. Das Stimmrecht kann nur
    persönlich oder für ein Mitglied unter Vorlage einer schriftlichen
    Vollmacht ausgeübt werden.
14. Ein Mitglied kann maximal drei Stimmen wahrnehmen.
15. Bei Abstimmungen entscheidet die einfache Mehrheit der abgegebenen Stimmen.  
    15.1. Stimmenthaltungen und ungültige Stimmen bleiben hierbei außer
          Betracht.  
16. Satzungsänderungen, Änderungen der Vereinszwecke und die Auflösung des
    Vereins können nur mit einer Mehrheit von 2/3 der anwesenden ordentlichen
    Mitglieder beschlossen werden.
17. Über die Beschlüsse der Mitgliederversammlung ist ein Protokoll
    anzufertigen, das von der Versammlungsleitung und der Schriftführung zu
    unterzeichnen ist.
18. Näheres regelt die Geschäftsordnung der Mitgliederversammlung.


### §14 Der Vorstand

1.  Der Vorstand besteht aus mindestens drei, maximal elf Vorstandsmitgliedern:  
    1.1. zwei Vorsitzenden,  
    1.2. dem/der Kassenwart\*in  
    1.3. bis zu acht regulären Vorstandsmitgliedern.  
2.  Der Vorstand im Sinn des §26 BGB besteht aus den Vorsitzenden und dem/der
    Kassenwart\*in. Sie vertreten den Verein gerichtlich und außergerichtlich.
3.  Zwei Vorstandsmitglieder vertreten den Verein gemeinsam.
4.  Der Vorstand wird von der Mitgliederversammlung auf die Dauer von einem
    Jahr gewählt.
5.  Vorstandsmitglieder können nur ordentliche Mitglieder des Vereins werden.
6.  Der Vorstand kann Vorstandsmitglieder außerhalb der regulären Wahlen aus
    wichtigem Grund abwählen. Wichtige Gründe sind insbesondere:  
    6.1. Rücktritt vom Amt,  
    6.2. Austritt oder Ausschluß aus dem Verein,  
    6.3. Geschäftsunfähigkeit durch Krankheit oder Unauffindbarkeit.  
7.  Eine Wiederwahl ist zulässig.
8.  Der Vorstand bleibt so lange im Amt, bis ein neuer Vorstand gewählt ist.
9.  Scheidet ein Mitglied des Vorstandes vorzeitig aus, so sind die
    verbleibenden Mitglieder des Vorstandes berechtigt ein Mitglied des Vereins
    bis zur Wahl eines Nachfolgers durch die Mitgliederversammlung in den
    Vorstand zu wählen.
10. Mit der Beendigung der Mitgliedschaft im Verein endet auch das Amt als
    Vorstand.
11. Dem Vorstand obliegt die Führung der laufenden Geschäfte des Vereins. Er
    hat insbesondere folgende Aufgaben:  
    11.1. die Einberufung und Vorbereitung der Mitgliederversammlung,  
    11.2. die Ausführung von Beschlüssen der Mitgliederversammlung,  
    11.3. die Verwaltung des Vereinsvermögens,  
    11.4. die Anfertigung eines schriftlichen Jahresberichts,  
    11.5. die Berufung und Aufl ösung von Teams.  
12. Die Beschlüsse des Vorstandes sind zu protokollieren und die
    Vereinsmitglieder über diese zu informieren.
13. Näheres regelt die Geschäftsordnung des Vorstandes.


### §15 Teams

1. Teams können von der Mitgliederversammlung oder dem Vorstand zur Erledigung
   einer bestimmten Aufgabe berufen werden.
2. Teams sind gegenüber dem bestellenden Gremium rechenschaftspflichtig.
3. Mitglieder von Teams können natürliche Personen, juristische Personen und
   Amtsträger von Amts wegen werden.
4. Teammitglieder können durch die Mitgliederversammlung, den Vorstand und
   das Team selbst berufen werden.
5. Zu der zugeordneten Aufgabe gesellen sich folgende:  
   5.1. Benennung einer Ansprechperson für die restlichen Organe des Vereins.  
   5.2. Festlegung einer Geschäftsordnung des Teams.  
6. Mitglieder von Teams können durch Beschluss der Mitgliederversammlung, des
   Vorstandes oder des Teams abberufen werden.
7. Teams können als Ganzes vom bestellenden Gremium oder der
   Mitgliederversammlung aufgelöst werden.
8. Näheres regeln die Rahmengeschäftsordnung für Teams und nachrangig die
   eigene Geschäftsordnung des Teams.


### §16 Beirat

1.  Mitglieder des Beirates können natürliche Personen und Amtsträger von Amts
    wegen werden.
2.  Mitglieder des Beirats können von allen Vereinsmitgliedern vorgeschlagen
    werden.
3.  Die Berufung erfolgt durch die Mitgliederversammlung.
4.  Die Amtszeit eines Mitgliedes des Beirates beträgt drei Jahre.
5.  Wiederberufung ist möglich.
6.  Die Tätigkeit als Beirat erfolgt ehrenamtlich und unentgeltlich.
7.  Der Beirat wählt aus seiner Mitte eine\*n Sprecher\*in.
8.  Beiratsmitglieder haben das Recht, an allen Sitzungen des Vereins
    teilzunehmen und sind über diese zu informieren.
9.  Der Beirat versammelt sich mindestens einmal im Geschäftsjahr. Der Vorstand
    lädt gemeinsam mit dem/der Sprecher\*in des Beirats zu den Versammlungen
    ein.
10. Aufgaben des Beirats:  
    10.1. Der Beirat berät den Verein in allen wichtigen Fragen.  
    10.2. Der Beirat wirbt für die Ideen und Ziele des Vereins in der
          Öffentlichkeit.  
    10.3. Der Beirat hat das Recht, Impulse und Anträge in die
          Mitgliederversammlung einzubringen.  
11. Mitglieder des Beirates können durch die Mitgliederversammlung des Vereins
    vorzeitig abberufen werden. Voraussetzung ist die Verletzung der Interessen
    des Vereins.


### §17 Kassenprüfung

1. Die Mitgliederversammlung wählt für die Dauer von einem Jahr mindestens zwei
   Kassenprüfer\*innen.
2. Diese dürfen nicht Mitglied des Vorstands sein.
3. Kassenprüfer dürfen nicht die Abrechnungen von Teams prüfen, in denen sie
   geschäftsführend tätig sind.
4. Wiederwahl ist zulässig.
5. Soll der Vorstand entlastet werden, ist der Mitgliederversammlung zwei
   Wochen vor deren Zusammentreten ein Kassenprüfbericht von zwei Kassenprüfern
   vorzulegen.


### §18 Auflösung des Vereins

Bei Auflösung oder Aufhebung des Vereins fällt das Vermögen des Vereins an den
Freifunk Rheinland e.V. oder einen anderen gemeinnützigen Verein, der sich im
Bereich der digitalen Volksbildung betätigt zur Umsetzung gemeinnütziger
Zwecke.

