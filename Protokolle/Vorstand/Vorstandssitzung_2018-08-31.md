Zeit: 31.8.2018, Start: 23:39 Uhr, Ende: 23:58 Uhr

Ort: Arne's WG, Aachen

Protokoll: Dennis Crakau

---

__Anwesend__
- Jan
- Arne
- Malte
- Gregor
- Dennis

---

# [TOP 1] Formalia

- Vorstand vollständig anwesend
- Einladung erfolgte ordnungsgemäß am 14.8.2018 bei der Vorstandssitzung.

# [TOP 2] Vorbereitung MV

- keine Anträge zur MV eingegangen

# [TOP 3] Nachbesprechung letztes Protokoll

- wurde nicht besprochen, weil dieses nicht fristgerecht eingegangen ist
  - inzwischen liegt ein Entwurf vor

# [TOP 4] Geld #8 Ehrenwert Aktionstag der Aachener Vereine

- lt FN kommt der Pavillon von Arne

# [TOP 5] Finanzantrag von Gregor

- Gregor hätte gerne 50€ Budget für Bürobedarf für die Vorbereitung/Durchführung der anstehenden MV
  - wurde einstimmig mit "JA" beschlossen
